using Godot;
using System;

namespace rz_frzbn.Singletons.InvItems.Items.ItemClass{

    public enum ItemTypes{
        Weapon,
        FoodItem,
        ConsumableItem,
        OtherItem,
    }

    public class ItemClass : Node {
    }
}